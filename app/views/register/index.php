<div class="container my-5">
    <div class="d-flex justify-content-center">
        <div class="col-xl-10 col-lg-12">
            <div class="card border-0 shadow-lg">
                <div class="card-bodyp-0">
                    <div class="row">
                        <div class="col-lg-5" style="background-image: url('<?= HREF; ?>/img/labe.jpeg'); background-size:cover;"></div>
                        <div class="col-lg-7 p-5">
                            <div class="m-5">
                                <div class="mb-4 text-center">
                                    <h5>Sign Up</h5>
                                    <p class="text-secondary">Join the community today!</p>
                                </div>
                                <form action="<?=BASEURL;?>/register/create" method="post">
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Username</label>
                                        <input type="text" class="form-control" placeholder="username" name="username" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Email</label>
                                        <input type="text" class="form-control" placeholder="email" name="email" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Password</label>
                                        <input type="password" class="form-control" placeholder="password" name="password" required>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn bg-active text-light mb-3" style="width: 100%;">Sign Up</button>
                                        <p class="text-secondary">Already have an account? <a href="<?= BASEURL;?>/login" class="text-decoration-none">Sign In</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>