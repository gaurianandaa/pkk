<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $data['title'] ?></title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <img src="" alt="">
            </div>
            <div class="col-lg-6">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" class="" placeholder="username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="text" class="" placeholder="password" name="password">
                    </div>
                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</body>

</html>