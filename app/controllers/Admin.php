<?php 

class Admin extends Controller{
    public function index(){
        $data['title'] = 'Admin';
        // $this->view('templates/header', $data);
        $this->view('templates/sidebar', $data);
        $this->view('admin/index', $data);
        $this->view('templates/endsidebar');
    }
}