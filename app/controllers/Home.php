<?php 

class Home extends Controller{
    public function index(){
        $data['title'] = 'Home';
        $this->view('home/index', $data);
    }
    public function login(){
        $this->view('login/index');
    }
}