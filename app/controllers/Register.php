<?php 

class Register extends Controller{
    public function index(){
        $data['title'] = 'Register';
        $this->view('register/index',$data);
    }
    public function create(){
        if ($this->model('User_model')->register($_POST) > 0) {
            header('Location: ' . BASEURL . '/login');
        } else {
            header('Location: ' . BASEURL . '/register/create');
        }
    }
}