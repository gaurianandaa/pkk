<?php 

class User_model{
    private $table = 'users';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getUser()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    public function register($data){
        $query = "INSERT INTO users(username, email, password) VALUES(:username, :email, :password)";
        $passwordHash = password_hash($data['password'], PASSWORD_DEFAULT);
        $this->db->query($query);
        $this->db->bind('username',$data['username']);
        $this->db->bind('email',$data['email']);
        $this->db->bind('password',$passwordHash);

        $this->db->execute();

        return 1;
    }
}